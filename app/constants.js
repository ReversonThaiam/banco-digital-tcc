import React from "react";
import {Image} from "react-native";
import {logoWhiteLabel} from './style/pallet';

export const Logo = ({moreStyle, imagem}) => {
    console.log(logoWhiteLabel)
    let style;
    imagem = !imagem  ? logoWhiteLabel: imagem
    if (Array.isArray(moreStyle)) {
        let newArray = [
            {
                resizeMode: 'contain',
                width: 200, height: 200
            }
        ];
        newArray = newArray.concat(moreStyle);
        style = newArray;
    } else {
        style = {
            ...{
                resizeMode: 'contain',
                width: 300, height: 300
            }, ...moreStyle
        }
    }
    return <Image source={logoWhiteLabel} style={style} />;
}

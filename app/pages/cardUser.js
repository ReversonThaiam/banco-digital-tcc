import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  Body,
  Container,
  Content,
  H1,
  Header,
  Left,
  List,
  Text,
  ListItem,
  View,
  H3,
  Footer,
  FooterTab,
  Right,
} from 'native-base';
import {
  StatusBar,
  TouchableOpacity,
  ActivityIndicator,
  TextInput,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {defaultTextColor, degrade_primario, lightButton} from '../style/pallet';
import theme from '../style/theme';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {SCLAlert, SCLAlertButton} from 'react-native-scl-alert';
import {Linking} from 'react-native';
import CreditCard from 'react-native-credit-card';
import {updateStatusCardStore} from '../store/cards';
import BouncyCheckbox from "react-native-bouncy-checkbox";





class CardUser extends Component {
  state = {
    loading: false,
    showAlertBlockCard: false,
    showAlertPassword: false,
    showAlertSuccess: false,
    id_cartao: '0',
    status_card: false,
  };
  static navigationOptions = ({navigation}) => {
    return {
      header: null,
    };
  };

  constructor(props) {
    super(props);
    this.card = this.props.navigation.getParam('card');
    this.card.id_usuario = this.props.account.conta.id_usuario;
    this.state.status_card = this.card.bloqueado
    this.state.id_cartao = this.card.id_cartao;
    //this.props.account.conta.id_usuario;
    console.log(this.state.status_card)
    console.log(props, 'props')
  }
  alertCloseCard() {
    this.setState({showAlertBlockCard: false});
  }
  alertClosePassword() {
    this.setState({showAlertPassword: false});
  }
  alertCloseSuccess() {
    this.setState({showAlertSuccess: false});
  }
  

  render() {

    console.log(this.card)
    const {card} = this;
    return (
      <Container style={{backgroundColor: degrade_primario}}>
        <Header style={[{height: 100, backgroundColor: degrade_primario}]}>
          <Left>
            <TouchableOpacity
              style={{marginTop: 20}}
              onPress={() => this.props.navigation.navigate('Dashboard')}>
              <AntDesign name={'left'} size={35} color={defaultTextColor} />
            </TouchableOpacity>
          </Left>
          <Body>
            <H1
              allowFontScaling={false}
              style={[
                theme.h1,
                {marginTop: 15},
                {marginLeft: Platform.OS === 'ios' ? wp('-50%') : 0},
              ]}>
              Cartão #{card.id}
            </H1>
          </Body>
        </Header>
        <StatusBar
          translucent
          barStyle="light-content"
          backgroundColor="transparent"
        />
        <Content>
          <View
            style={[
              theme.centerAll,
              {
                flex: 1,
                paddingBottom: 10,
              },
            ]}>
            <Text
              allowFontScaling={false}
              style={[theme.h3_light, theme.centerAll, {marginHorizontal: 10}]}>
              Dados do cartão
            </Text>
          </View>

          <View
            style={{
              flex: 1,

              justifyContent: 'center',
              alignContent: 'center',
              alignItems: 'center',
            }}>
            <CreditCard
              style={{borderWidth: 2, borderColor: 'white'}}
              type={''}
              imageFront={require('../../assets/icones/cartao_front_ppbank.png')}
              imageBack={require('../../assets/icones/cartao_back_ppbank.png')}
              shiny={false}
              bar={false}
              focused={false}
              number={card.pin + '******' + card.ultimos_digitos}
              name={card.nome_impresso}
              expiry={'1218'}
              cvc={'123'}
            />
          </View>
          <View style={{height: 15}} />
             
          <View style={theme.centerAll, [{flexDirection: 'column', marginBottom: 90, marginTop: 20}]}>
            <View style={[{flexDirection: 'row'}, theme.centerAll]}>
              <View
                style={
                  ([],
                  {
                    height: 60,
                    width: 160,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderColor: '#9e9e9e',
                    borderWidth: 2,
                    marginRight: 20,
                    borderRadius: 5,
                  })
                }>
                <Text style={{color: 'white', padding: 5}}>Limite</Text>
                <Text style={{color: 'white', paddingLeft: 5, marginBottom: 10}}>R$ 200,00</Text>
              </View>

              <View
                style={
                  ([theme.centerAll],
                  {
                    flexDirection: 'column',
                    height: 60,
                    width: 160,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderColor: '#9e9e9e',
                    borderWidth: 2,
                    
                    borderRadius: 5,
                  })
                }>
                  {this.state.status_card && (
                  <View style={{alignItems: 'center'}}>
                  <Text style={[{color: 'white', paddingTop: 5}]}>Cartão ativado</Text>
                  <BouncyCheckbox fillColor="green" style={[{marginBottom: 10}]} isChecked={this.state.status_card} onPress={() => this.blockCard()} />
                  </View>
                  )}

                  {!this.state.status_card && (
                    <View style={{alignItems: 'center'}}>
                    <Text style={[{color: 'white', paddingTop: 5}]}>Cartão bloqueado</Text>
                    <BouncyCheckbox fillColor="red" style={[{marginBottom: 10}]} isChecked={this.state.status_card} onPress={() => this.blockCard()} />
                    </View>
                  )}
                

              </View>
            </View>
          </View>

          <View
            style={{
              flex: 1,
              justifyContent: 'flex-end',
              marginTop: 15,
            }}>
            <TouchableOpacity
              onPress={() =>  this.props.navigation.navigate('History')}
              // disabled={this.state.status_card}
              style={[theme.buttonCartoes, theme.centerAll]}>
              {this.state.loading && (
                <ActivityIndicator
                  style={[theme.centerAll, {marginTop: 8}]}
                  color={lightButton}
                  size={Platform.OS === 'ios' ? 'large' : 50}
                />
              )}
              <H3
                allowFontScaling={false}
                style={[theme.centerAll, theme.h3_black]}>
                Ver movimentações
              </H3>
            </TouchableOpacity>
          </View>

          

          <View
            style={{
              flex: 1,
              justifyContent: 'flex-end',
              marginTop: 15,
             
            }}>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('NewPasswordCard', {
                  new_card: this.card,
                })
              }
              style={[theme.buttonCartoes, theme.centerAll]}>
              {this.state.loading && (
                <ActivityIndicator
                  style={[theme.centerAll, {marginTop: 8}]}
                  color={lightButton}
                  size={Platform.OS === 'ios' ? 'large' : 50}
                />
              )}
              <H3
                allowFontScaling={false}
                style={[theme.centerAll, theme.h3_black]}>
                Alterar senha
              </H3>
            </TouchableOpacity>
          </View>
          <View>
            <SCLAlert
              show={this.state.showAlertBlockCard}
              onRequestClose={this.alertCloseCard.bind(this)}
              theme={this.state.alert.theme || 'info'}
              title={this.state.alert.title || ''}
              subtitle={this.state.alert.subtitle || ''}
              headerIconComponent={
                this.state.alert.icon || (
                  <AntDesign
                    name={this.state.alert.icone || 'info'}
                    size={35}
                    color={'#ffffff'}
                  />
                )
              }>
              {this.state.status_card && (
                <SCLAlertButton
                  theme={this.state.alert.theme || 'info'}
                  onPress={() => this.bloquearCartao(false)}>
                  Bloquear
                </SCLAlertButton>
              )}
              {!this.state.status_card && (
                <SCLAlertButton
                  theme={'danger'}
                  onPress={() => this.bloquearCartao(true)}>
                  Ativar
                </SCLAlertButton>
              )}
            </SCLAlert>
            
            <SCLAlert
              show={this.state.showAlertSuccess}
              onRequestClose={this.alertCloseSuccess.bind(this)}
              theme={this.state.alert.theme || 'info'}
              title={this.state.alert.title || ''}
              subtitle={this.state.alert.subtitle || ''}
              headerIconComponent={
                this.state.alert.icon || (
                  <AntDesign
                    name={this.state.alert.icone || 'info'}
                    size={35}
                    color={'#ffffff'}
                  />
                )
              }>
              <SCLAlertButton
                theme={'success'}
                onPress={() => this.alertCloseSuccess(this)}>
                OK
              </SCLAlertButton>
            </SCLAlert>
          </View>
          <View />
        </Content>
      </Container>
    );
  }

  blockCard() {
    this.setState(data => {
      data.alert = {
        title: 'Atenção',
        subtitle: 'O que deseja fazer com o cartão?',
        theme: 'danger',
        icone: 'closecircle',
      };
      data.showAlertBlockCard = true;
      return data;
    });
    this.setState({loading: false});
    return false;
  }
  updatePassword() {
    this.setState({showAlertPassword: true});
  }
  
  bloquearCartao(bool) {
    this.setState({showAlertBlockCard: false});
    this.setState({status_card: bool});
    console.log(this.state.status_card, 'status')
    this.updateStatusCardStore(bool);
  }

  async updateStatusCardStore(bool) {
    const {updateStatusCardStore} = this.props;
    this.setState({loading: true});

    let result = await updateStatusCardStore({
      id_cartao: this.state.id_cartao,
      bloquear: bool,
    });
    if (result.status === 'success') {
      this.setState(data => {
        data.alert = {
          title: 'Sucesso !',
          subtitle: 'Cartão atualizado com sucesso',
          theme: 'success',
          icone: 'closecircle',
        };
        data.showAlertSuccess = true;
        return data;
      });
    } else {
      this.setState(data => {
        data.alert = {
          title: 'Oops',
          subtitle: 'Erro ao atualizar status cartão',
          theme: 'danger',
          icone: 'closecircle',
        };
        data.showAlert = true;
        return data;
      });
    }
    this.setState({loading: false});
  }
}

const mapStateToProps = state => {
  return {
    account: state.account ? state.account.data : null,
    bankSettings: state.bankSettings,
    favorites: state.favorites,
    banks: state.banks,
  };
};
const mapDispatchToProps = dispatch => {
  return bindActionCreators({updateStatusCardStore}, dispatch);
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CardUser);
